//Using SDL and standard IO
#include <SDL.h>
#include <stdio.h>
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <GL\glew.h>
#include <SDL_opengl.h>

#include <GL/glu.h>

#include "stafx.h"
#include "demo.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const Uint32 fps = 60;
const Uint32 minimumFrameTime = 1000 / fps;

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif


int main(int argc, char* args[])
{

	Demo* demo;
	//The window we'll be rendering to
	SDL_Window* window = NULL;
	SDL_GLContext _glcontext;

	//The surface contained by the window
	SDL_Surface* screenSurface = NULL;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return 0;
	}

	//Create window
	window = SDL_CreateWindow("SDL Library Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	if (window == NULL)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
	}

	//Get window surface
	screenSurface = SDL_GetWindowSurface(window);
	//Update the surface
	SDL_UpdateWindowSurface(window);

	_glcontext = SDL_GL_CreateContext(window);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	glEnable(GL_MULTISAMPLE);
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		// Problem: glewInit failed, something is seriously wrong.
		printf("glewInit failed: : %s\n", glewGetErrorString(err));
		exit(1);
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (!renderer) {
		printf("Render context could not be created! SDL_Error: %s\n", SDL_GetError());
		return 1;
	}

	bool quit = false;
	SDL_Event event;
	Uint32 frameTime;
	Uint32 lastFrameTime = 0;
	Uint32 deltaTime = 0;

	ShaderManager shaderManager = ShaderManager();
	shaderManager.InitShaders();
	demo = new Demo();
	demo->Init(shaderManager);

	//Game Loop
	while (!quit) {
		while (!quit && SDL_PollEvent(&event))
		{
			//Events
			if (event.key.keysym.sym == SDLK_ESCAPE)
			{
				quit = true;
			}

			demo->KeyHandler(event);
		}

		frameTime = SDL_GetTicks();
		deltaTime = frameTime - lastFrameTime;
		lastFrameTime = frameTime;
		//std::cerr << "Delta time: " << deltaTime << std::endl;

		//Logic
		demo->Update(deltaTime);


		//Rendering
		SDL_RenderClear(renderer);
		glClear(GL_COLOR_BUFFER_BIT);
		
		demo->Render(false);

		SDL_GL_SwapWindow(window);

		//delay to enforce fps
		if ((SDL_GetTicks() - frameTime) < minimumFrameTime)
			SDL_Delay(minimumFrameTime - (SDL_GetTicks() - frameTime));
	}

	delete demo;

	//Destroy window and rendering context
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_GL_DeleteContext(_glcontext);


	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}