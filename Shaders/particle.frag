// simple fragment shader simple.frag
#version 140
// OpenGL 3.2 replace above with #version 150
// Some drivers require the following
precision highp float;
 

uniform sampler2D texture0;
uniform sampler1D texture1;

in vec4 ex_Color;
in float ex_TexCoord;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor
 
void main(void) {
	
	if(ex_Color.y == 0) {
		discard;
	}

	vec4 fragCol2 = texture(texture0, gl_PointCoord);

	gl_FragColor = fragCol2 * texture(texture1, ex_TexCoord) * ex_Color;
}
