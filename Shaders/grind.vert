// simple vertex shader - simple.vert
#version 140
// OpenGL 3.2 replace above with #version 150


uniform mat4x4 modelview;
uniform mat4x4 projection;



in vec3 in_Position;
in vec3 in_Color;
in vec2 in_TexCoord;

out vec4 ex_Color;
out vec2 ex_TexCoord;

// simple shader program
// particle vertex program
void main(void) {
	
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;

	gl_PointSize = 25.0;

	ex_Color = vec4(in_Color, 1.0);

}
