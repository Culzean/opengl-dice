// simple fragment shader simple.frag
#version 140
// OpenGL 3.2 replace above with #version 150
// Some drivers require the following
precision highp float;
 

uniform sampler2D texture0;

in vec4 ex_Color;
in vec2 ex_TexCoord;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor
 
void main(void) {
	gl_FragColor = ex_Color * texture2D(texture0, gl_PointCoord);
	//gl_FragColor = vec4(gl_PointCoord, 0.0f,1.0f);
	//gl_FragColor = texture(texture0, vec2(1.0));
	//gl_FragColor = ex_Color;
}
