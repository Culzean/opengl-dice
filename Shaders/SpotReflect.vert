/*
	Author:		Floyd Chitalu
	BannerID:	B00203579
*/

#version 130

uniform mat4 projection;
uniform mat4 modelview;

struct light {
	vec4 position;
	vec4 diffuse;
	vec4 specular;
	vec4 ambient;
	
	float constant_attenuation;
	float linear_attenuation;
	float quadratic_attenuation;

	float spotCutOff;
	vec3 spotDirection;
	float spotExponent;
	float Eta;
};

uniform light light0;

in vec3 in_Position;
in vec3 in_Normal;
out vec4 ex_Color;

out vec3 ex_N;
out vec3 lightPos;
out vec3 lightDir;

out vec4 ex_position;
out vec3 ex_ReflectionDirection;
out vec3 ex_RefractionDirection;

void main(void){
	ex_N = normalize(modelview * vec4(in_Normal, 0.0)).xyz;	
	
	lightPos = (light0.position).xyz;

	ex_position = modelview * vec4(in_Position, 1.0);

	gl_Position = projection * ex_position;	

	lightDir = (light0.position - ex_position).xyz;

	ex_ReflectionDirection = reflect(ex_position.xyz, ex_N );

	vec3 ex_V = normalize(-ex_position.xyz);
	ex_RefractionDirection = refract(ex_position.xyz, ex_N, light0.Eta );
	
	ex_Color = vec4(1.0, 0.0, 1.0, 1.0);
}
