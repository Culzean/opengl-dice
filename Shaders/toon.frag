//toon.frag
#version 130

uniform sampler2D textureUnit0;

in vec2 ex_TexCoord;
in vec4 ex_Color;

void main(void){

	vec4 litColour = ex_Color;
	vec4 shade1 = smoothstep(vec4(0.2),vec4(0.21),litColour);

	vec4 shade2 = smoothstep(vec4(0.4),vec4(0.41),litColour);

	vec4 shade3 = smoothstep(vec4(0.8),vec4(0.81),litColour);

	vec4 colour = max( max(0.3*shade1, 0.5*shade2), shade3);
	gl_FragColor = colour * texture2D(textureUnit0, ex_TexCoord);
}
