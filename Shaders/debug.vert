// A debug shader to draw lines
#version 130
// OpenGL 3.2 replace above with #version 150

uniform mat4x4 MV;
uniform mat4x4 projection;

in vec3 in_Norm;
//line could be normal, velocity vector, or axis
in vec3 in_Color;


out vec3 ex_Color;

// simple shader program
// multiply each vertex position by the MVP matrix
void main(void) {
    // draw lines in correct MVP pos
	vec4 vertexPosition = MV * vec4(in_Norm,1.0);
	gl_Position = projection * vertexPosition;

	ex_Color = in_Color;

}
