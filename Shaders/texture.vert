//simple vertex shader - simple.vert
#version 130

// OpenGl 3.2 replace above with #version 150

uniform mat4 modelview;
uniform mat4 projection;
uniform vec4 lightPosition;

in vec3 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoord;
out vec3 ex_N;
out vec3 ex_V;
out vec3 ex_L;
out vec2 ex_TexCoord;



//multiply each vertex position by the MVP matrix
//and find V, L, N vectors for the fragment shader
void main(void)
{
	//vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position, 1.0);
	gl_Position = projection * vertexPosition;

	//Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition.xyz);

	//Vertex normal in eye coordinates
	//if(in_Normal.x < 0 || in_Normal.y < 0 || in_Normal.z < 0)
	//	in_Normal = -in_Normal;
	ex_N = normalize(modelview * vec4(in_Normal, 0.0)).xyz;

	//L- to light oruce from vertex
	ex_L = normalize(lightPosition.xyz - vertexPosition.xyz);

	//ex_TexCoord = in_Position.xy + vec2(0.5);

	ex_TexCoord = in_TexCoord;

}