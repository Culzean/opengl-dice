#include "Demo.h"

Demo::Demo(void)
{
}

Demo::~Demo(void)
{
}

void Demo::Init(ShaderManager shaderManager)
{
	FREE_CAMERA = false;
	UPDATING = false;
	count = 0;
	timer = 0;
	prevUpdating = false;
	selection = 0;

	textureManager.InitTextures();

	projectionStack.loadMatrix(glm::perspective(45.0f, 4.0f / 3.0f, 0.5f, 5000.f));
	
	/*terrain = new Terrain();
	terrain->ApplyTexture(textureManager.TERRAIN);
	terrain->ApplyShader(shaderManager.TEXTURE);
	//terrain->Generate(100, 100, 25, 25);
	terrain->Generate(256,256, 25, 25);
	terrainShader = shaderManager.TEXTURE;*/


	//make an init method for the camera
	rotXAngle = 0.0f;
	rotYAngle = 0.0f;
	camera = new Camera(modelviewStack.getMatrix(), projectionStack.getMatrix());
	camera->SetProperties(camera->CAMERA_FLIGHT);

	spawnPos = vec3(0.f, 12.f, -8.f);
	ball = new Ball("Res/dice.obj", vec3(0.f, 3.f, 0.f));
	ball->ApplyTexture(textureManager.DICE, textureManager.DICE);
	ballShader = shaderManager.TEXTURE;
	ball->AssignShader(ballShader);
	camera->SetPosition(vec3(0.f, 12.f, 8.f));
	cameraMatrix = glm::lookAt(camera->GetPosition(), ball->GetPosition(), glm::vec3(0.0f,1.0f, 0.0f));



	skyBox = new SkyBox();
	skyBox->ApplyTexture(textureManager.SKYBOX);
	skyBox->AssignShader(shaderManager.SKYBOX);
	skyBox->Init();

	dice = std::vector<Ball*>();

	std::cout << "Demo state initialized without fault." << std::endl;
}

void Demo::Update(GLfloat dt)
{
	//convert into seconds
	dt /= 1000;

	if (UPDATING)
	{
		timer += dt;

		glm::vec3 partsLooks = camera->GetViewDirection();
		UpdateCamera(dt);
	}
	else
	{
		UPDATING = false;
	}

	ball->Update();
	ball->UpdatePhysics(dt);
	for (int i = 0; i < dice.size(); i++) {
		dice[i]->UpdatePhysics(dt);
		dice[i]->Update();
	}

	prevUpdating = UPDATING;
}

void Demo::UpdateCamera(GLfloat dt)
{
	if (!FREE_CAMERA)
	{
		camera->SetPosition(ball->GetPosition());
		
		camera->Move(glm::normalize(currentLookVec), -17.5f);

		cameraMatrix = glm::lookAt(camera->GetPosition(), ball->GetPosition(), glm::vec3(0.0f,1.0f, 0.0f));
	}
}

void Demo::Render(bool WIRE_FRAME)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if (WIRE_FRAME)
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	else
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	modelviewStack.loadIdentity();
	if(FREE_CAMERA)
		modelviewStack.push(camera->GetViewMatrix());
	else
		modelviewStack.push(cameraMatrix);
		
		glDisable(GL_DEPTH_TEST);
		skyBox->Render(projectionStack, modelviewStack, camera->GetPosition(), WIRE_FRAME);
		glEnable(GL_DEPTH_TEST);

		ball->Render(projectionStack, modelviewStack, WIRE_FRAME);

		for (int i = 0; i < dice.size(); i++) {
			dice[i]->Render(projectionStack, modelviewStack, WIRE_FRAME);
		}

	modelviewStack.pop();


		glFinish();
}

void Demo::KeyHandler( SDL_Event sdlEvent )
{
	if(sdlEvent.type == SDL_KEYDOWN)
	{
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_1:
				if (!FREE_CAMERA)
				{
					std::cout << "FREE CAMERA" << std::endl;
					FREE_CAMERA = true;
				}
				else
				{
					std::cout << "FIXED CAMERA" << std::endl;
					FREE_CAMERA = false;
				}
			break;
			case SDLK_2:
				if (!UPDATING)
				{
					UPDATING = true;
					std::cout << "UPDATING" << std::endl;
				}
				else
				{
					UPDATING = false;
					std::cout << "PAUSED" << std::endl;
				}
			break;
			case SDLK_SPACE:
				AddDice();
				
			break;
			case SDLK_DOWN:
				if (UPDATING)
				{
					ball->SetNegZ(true);
				}
			break;
			case SDLK_LEFT:
				if (UPDATING)
				{
					ball->SetNegX(true);
				}
			break;
			case SDLK_RIGHT:
				if (UPDATING)
				{
					ball->SetPosX(true);
				}
			break;
			case SDLK_RETURN:
				if (UPDATING)
				{
					ball->InputBoost(true);
				}

			break;
			default:
				break;
		}		
	}
	else if(sdlEvent.type == SDL_KEYUP)
	{
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_UP:
				if (UPDATING)
				{
					ball->SetPosZ(false);
				}
			break;
			case SDLK_DOWN:
				if (UPDATING)
				{
					ball->SetNegZ(false);
				}
			break;
			case SDLK_LEFT:
				if (UPDATING)
				{
					ball->SetNegX(false);
				}
			break;
			case SDLK_RIGHT:
				if (UPDATING)
				{
					ball->SetPosX(false);
				}
			break;
			case SDLK_RETURN:
				if (UPDATING)
				{
					ball->InputBoost(false);
				}

			break;
			default:
				break;
		}
	}
}

void Demo::AddDice() {

	glm::vec3 actualPos = glm::sphericalRand(4.6f) + spawnPos;
	Ball* newDice = new Ball("Res/dice.obj", actualPos);
	newDice->ApplyTexture(textureManager.DICE, textureManager.DICE);
	newDice->AssignShader(ballShader);

	dice.push_back(newDice);
}

void Demo::Cleanup(void)
{
	ball->Cleanup();
	delete ball;
	delete camera;
	delete skyBox;
	textureManager.cleanUp();
	for (int i = 0; i < dice.size(); i++) {
		delete dice[i];
	}
	dice.clear();

}