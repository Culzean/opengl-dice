#include "MatrixStack.h"

MatrixStack::MatrixStack(){
	identityMatrix = glm::mat4(1.0);
	stack.push_back(identityMatrix);
	health = GOOD;
}

MatrixStack::MatrixStack(glm::mat4 _matrix){
	identityMatrix = glm::mat4(1.0);
	stack.push_back(identityMatrix);
	stack.push_back(_matrix);
	health = GOOD;
}

MatrixStack::~MatrixStack(){
	stack.erase(stack.begin(), stack.end()-1);
}

void MatrixStack::clearStack(){
	int size = stack.size();
	while(size > 0){
		pop();
		size = stack.size();
	}
}

void MatrixStack::loadIdentity(){
	int count = stack.size();
	while(count > 1){
		stack.pop_back();
		count = stack.size();
	}
}

void MatrixStack::loadMatrix(glm::mat4 mMatrix){
	loadIdentity();
	stack.push_back(mMatrix); 
}

void MatrixStack::multiplyMatrix(const glm::mat4 mMatrix){ 
	stack.back() *= mMatrix;
}

void MatrixStack::scale(float x, float y, float z){
	stack.back() = glm::scale(stack.back(), vec3(x, y, z));
}

void MatrixStack::scale(const vec3 in){
	stack.back() = glm::scale(stack.back(), in);
}

void MatrixStack::translate(float x, float y, float z){
	stack.back() = glm::translate(stack.back(), vec3(x, y, z));
}

void MatrixStack::translate(const vec3 in){
	stack.back() = glm::translate(stack.back(), in);
}

void MatrixStack::rotate(float ang, float x, float y, float z){
	stack.back() = glm::rotate(stack.back(), ang, vec3(x, y, z));
}

void MatrixStack::rotate(float ang, const vec3 in){
	stack.back() = glm::rotate(stack.back(), ang, in);
}

void MatrixStack::push(){
	glm::mat4 newMatrix = glm::mat4(1.0);
	stack.push_back(newMatrix);
}

void MatrixStack::push(glm::mat4 mMatrix){
	//glm::mat4 temp = glm::mat4(1.0);
	mMatrix *= stack.back();
	stack.push_back(mMatrix);
}

// remember pop_back does not delete/ free the memory. Need to fix this method eradicate mem leak
int MatrixStack::pop(){
	if(stack.size() > 0){
		stack.pop_back();
	}else{
		health = STACK_UNDERFLOW;
		return health;
	}
	return health;
}

glm::mat4 MatrixStack::getMatrix(){
	return stack.back();
}

int MatrixStack::getSize(){
	return stack.size(); //i think returning a class variable is best here
}

STACK_ERRORS MatrixStack::getLastHealth(){
	STACK_ERRORS temp = health;
	health = GOOD;
	return temp; 
}

STACK_ERRORS MatrixStack::getCurrentHealth(){
	return health;
}