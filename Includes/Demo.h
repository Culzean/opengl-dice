#ifndef DEMO_H
#define DEMO_H

#include <iostream>
#include <cstdlib>

#include "stafx.h"
#include "State.h"
#include "Ball.h"

class Demo
{
	public:
		Demo(void);
		~Demo(void);

		void Init(ShaderManager shaderManager);
		void Render(bool WIRE_FRAME);
		void KeyHandler( SDL_Event sdlEvent);
		void Update(GLfloat dt);
		void UpdateCamera(GLfloat dt);
		glm::quat RotateTowards(glm::quat q1, glm::quat q2, float maxAngle);
		void Cleanup(void);
		int GetSelection(void) {return selection;}
		void SetSelection(int value) {selection = value;}
		int GetMusic(void) {return 0;}
		float GetTime(void) { return timer;}
		void AddTime(float time) {;}

		friend class Ball;

	private:
		void AddDice();
		bool FREE_CAMERA;
		bool UPDATING;
		bool BALL_MOVING;


		TextureManager textureManager;

		glm::mat4x4 cameraMatrix;
		glm::vec3 currentLookVec;
		
		
		MatrixStack projectionStack;
		MatrixStack modelviewStack;

		Ball					*ball;
		Camera					*camera;
		SkyBox					*skyBox;
		GLuint					terrainShader;
		GLuint					ballShader;

		float rotXAngle;
		float rotYAngle;
		float prevXCameraAngle;

		int count;
		bool prevUpdating;
		float timer;
		int selection;

		glm::vec3 spawnPos;
		std::vector<Ball*> dice;
};

#endif