//
////////////////////////////////////////////////
//
//included with Comp 06083 project
//by B0022604
#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H


#include <map>
#include <string>

#include <iostream>
#include <assert.h>
#include <SDL_image.h>

#include "stafx.h"
#include "DataLoader.h"


class TextureManager {
private:
	static TextureManager*		pTexManager;
	std::map<std::string, GLuint> Texmap;
	std::map<std::string, GLuint>::iterator index;
	static int lastID;
	DataLoader loader;
public:
	//method to get singleton instance
	static TextureManager* GetInstance(){
	if(!pTexManager)
		pTexManager = new TextureManager();
	return pTexManager;
	
	}
	static void DestroyInstance(){
		if(pTexManager)
			delete pTexManager;	pTexManager = NULL;

	}
	~TextureManager();
	TextureManager();
	void InitTextures(void);
	GLuint Load1DTexture(char *fname);
	GLuint LoadTexBMP(char *fname);
	GLuint LoadTexPNG( const char* fname);
	GLuint loadTexture(const char* fname);
	GLuint LoadCubeMapTexPNG(const char *fname[]);
	GLuint LoadCubeMapTexBMP(const char *fname[]);
	GLuint getTexID(const char* fname);
	void cleanUp();

	GLuint SKYBOX;
	GLuint WATERBOX;
	GLuint TERRAIN;
	GLuint DICE;
	GLuint BALL;
	GLuint TRACK;
};

#endif