////////////////////////////////////////////////
//
//Design and Plan - Ball Drop
//by B00226804 & 

#pragma once

#include <stdio.h>
#include <iostream>
#include <ctime>

#include <math.h>

#include <GL\glew.h>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <sstream>

#include "RenderHeaders.h"

#include "TextureManager.h"
#include "Camera.h"
#include "SkyBox.h"
#include "MatrixStack.h"
#include "ShaderManager.h"

