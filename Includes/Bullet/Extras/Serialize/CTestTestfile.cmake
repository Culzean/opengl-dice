# CMake generated Testfile for 
# Source directory: E:/Libraries/bullet3-master/bullet3-master/Extras/Serialize
# Build directory: E:/Projects/Dice/Dice/Includes/Bullet/Extras/Serialize
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(BulletFileLoader)
subdirs(BulletXmlWorldImporter)
subdirs(BulletWorldImporter)
