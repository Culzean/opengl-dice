# CMake generated Testfile for 
# Source directory: E:/Libraries/bullet3-master/bullet3-master/Extras
# Build directory: E:/Projects/Dice/Dice/Includes/Bullet/Extras
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(InverseDynamics)
subdirs(BulletRobotics)
subdirs(obj2sdf)
subdirs(Serialize)
subdirs(ConvexDecomposition)
subdirs(HACD)
subdirs(GIMPACTUtils)
