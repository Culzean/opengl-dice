# CMake generated Testfile for 
# Source directory: E:/Libraries/bullet3-master/bullet3-master/examples
# Build directory: E:/Projects/Dice/Dice/Includes/Bullet/examples
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(HelloWorld)
subdirs(BasicDemo)
subdirs(ExampleBrowser)
subdirs(RobotSimulator)
subdirs(SharedMemory)
subdirs(ThirdPartyLibs/Gwen)
subdirs(ThirdPartyLibs/BussIK)
subdirs(ThirdPartyLibs/clsocket)
subdirs(OpenGLWindow)
