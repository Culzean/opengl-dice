# CMake generated Testfile for 
# Source directory: E:/Libraries/bullet3-master/bullet3-master
# Build directory: E:/Projects/Dice/Dice/Includes/Bullet
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(examples)
subdirs(Extras)
subdirs(src)
subdirs(test)
