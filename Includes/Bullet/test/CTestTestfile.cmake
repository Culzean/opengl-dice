# CMake generated Testfile for 
# Source directory: E:/Libraries/bullet3-master/bullet3-master/test
# Build directory: E:/Projects/Dice/Dice/Includes/Bullet/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(InverseDynamics)
subdirs(SharedMemory)
subdirs(gtest-1.7.0)
subdirs(collision)
subdirs(BulletDynamics)
