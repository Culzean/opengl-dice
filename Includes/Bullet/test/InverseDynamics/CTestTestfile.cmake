# CMake generated Testfile for 
# Source directory: E:/Libraries/bullet3-master/bullet3-master/test/InverseDynamics
# Build directory: E:/Projects/Dice/Dice/Includes/Bullet/test/InverseDynamics
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(Test_BulletInverseDynamicsJacobian_PASS "Test_BulletInverseDynamicsJacobian")
add_test(Test_BulletInverseForwardDynamics_PASS "Test_BulletInverseForwardDynamics")
add_test(Test_BulletInverseDynamics_PASS "Test_BulletInverseDynamics")
