#ifndef MESH_H
#define MESH_H

#include <GL\glew.h>

#include "Renderable.h"

#include "ShaderManager.h"
#include "Frame.h"
#include "MatrixStack.h"

class ShaderManager;

/*
Much of this class is code handling a 3d mesh taken from last years project.
much a great deal of refitting has been carried out to get this up
to sdl glm standard and working with current shares plus the rendering heirachy

*/

class Mesh : public Renderable {

public:
	Mesh();
	virtual ~Mesh();

	virtual void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME);
	virtual void Update();
	virtual void Init();

	void createVertices(GLfloat iWidth, GLfloat iDepth, GLfloat iMaxWidth, GLfloat iMaxDepth);

	void BeginMesh(GLuint nMaxVerts);
	void EndMesh();
	void AddTriangle(glm::vec3 verts[3], glm::vec3 norms[3], glm::vec2 texCoords[3]);
	void AddIndexedTriangle(std::vector< glm::vec3 > pVerts, glm::vec3 norms[3], glm::vec2 texCoords[3]);
	void AddQuad(glm::vec3 _verts[4], glm::vec2 _texCoords[4]);

	void SetFrame(glm::vec3 _pos)				{	frame.SetOrigin(_pos);	};
	Frame		getFrame()						{	return frame;	};

private:

	bool loadData();

	bool comparePoints( glm::vec3 &p0, glm::vec3 &p1, GLfloat close );
	bool comparePoints( glm::vec2 &p0, glm::vec2 &p1, GLfloat close );

	glm::vec3 findNorm(const glm::vec3 p0, const glm::vec3 p1, const glm::vec3 p2) {
		glm::vec3 va = (p0 - p1);
		glm::vec3 vb = (p2 - p1);
		return glm::cross(va, vb); }

protected:
	Frame		frame;
};

#endif