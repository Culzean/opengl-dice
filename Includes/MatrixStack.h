#ifndef MATRIXSTACK_H_INCLUDED
#define MATRIXSTACK_H_INCLUDED

#include <GL\glew.h>
#include <glm/glm.hpp>
#include <glm/gtc\matrix_transform.hpp>
#include <glm/gtc\type_ptr.hpp>

#include <vector>

using namespace glm;

enum STACK_ERRORS{
	GOOD = 0,
	STACK_UNDERFLOW,
	STACK_OVERFLOW,
};

class MatrixStack{
public:
	MatrixStack();
	MatrixStack(glm::mat4 _matrix);
	virtual ~MatrixStack();
	void push();
	void push(mat4 mMatrix);
	int pop();
	void loadIdentity();
	void loadMatrix(mat4 mMatrix);
	void multiplyMatrix(const mat4 mMatrix);
	void scale(float x, float y, float z);
	void scale(vec3 in);
	void translate(float x, float y, float z);
	void translate(vec3 in);
	void rotate(float ang, float x, float y, float z);
	void rotate(float ang, vec3 in);
	mat4 getMatrix();
	int getSize();
	STACK_ERRORS getLastHealth();
	STACK_ERRORS getCurrentHealth();
protected:

private:
	void clearStack();
	mat4					identityMatrix;
	std::vector<mat4>		stack; //the actual container
	STACK_ERRORS			health;
};
#endif