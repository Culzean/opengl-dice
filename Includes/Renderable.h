#pragma once

#ifndef	RENDERABLE_H
#define RENDERABLE_H

#include <string>

#include <SDL.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <GL\glew.h>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc\matrix_transform.hpp>
#include <glm/gtc\type_ptr.hpp>

class ShaderManager;
class MatrixStack;
class Frame;

class Renderable 
{
public:
	Renderable();
	virtual ~Renderable()=0;

	virtual void Init()=0;
	virtual void Update(float dt) =0;
	virtual void Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME );

	GLuint getTexID()			{	return texID;	};
	GLuint getShaderID()		{	return shaderID;	}
	GLuint getShaderType()		{	return shaderType;	}

	void AssignShader(GLuint type)			{	shaderID = type;	}
	void setTexID(GLuint ID )				{	texID = ID;	}
	void setTexName(std::string fname)		{	texName = fname;	}	

	void LoadMaterials(GLfloat* mat);

	///material properties
	glm::vec4 getAmbient()						{	return ambient;		}
	glm::vec4 getDiffuse()						{	return diffuse;		}
	glm::vec4 getSpecular()						{	return specular;	}
	GLfloat getShininess()						{	return shininess;	}

	void setAmbient( glm::vec4 value )			{	ambient = value;	}
	void setDiffuse( glm::vec4 value )			{	diffuse = value;	}
	void setSpecular( glm::vec4 value )			{	specular = value;	}
	void setShininess( GLfloat value )			{	shininess = value;	}

protected:

	//rendering data
	//used by inheriting class
	
	GLuint vao;
	GLuint*		vbo;
	GLuint		iNoBuffers;
	
	GLuint		iNoFaces;
	GLuint		iNoVerts;
	GLuint		nMaxVerts;

	std::vector< glm::vec3 > pVerts;
	std::vector< glm::vec3 > pNorms;
	std::vector< glm::vec2 > pTexCoords;
	std::vector< GLuint > pIndexes;

	GLuint shaderID;
	GLuint shaderType;
	GLuint texID;
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	GLfloat shininess;
	std::string texName;

};

#endif